<?php
require_once("function.php");
$user = detect_session(); // tester si l'utilisateur est identifié ?

// Constantes
define('TARGET', 'files/');    // Repertoire cible
define('MAX_SIZE', 100000);    // Taille max en octets du fichier
define('WIDTH_MAX', 3000);    // Largeur max de l'image en pixels
define('HEIGHT_MAX', 3000);    // Hauteur max de l'image en pixels
// Tableaux de donnees
$tabExt = array('jpg','gif','png','jpeg');    // Extensions autorisees
$infosImg = array();
// Variables

$extension = '';
$message = '';
$nomImage = '';
 
/************************************************************
 * Creation du repertoire cible si inexistant
 *************************************************************/
if( !is_dir(TARGET) ) {
  if( !mkdir(TARGET, 0755) ) {
    exit('Erreur : le répertoire cible ne peut-être créé ! Vérifiez que vous diposiez des droits suffisants pour le faire ou créez le manuellement !');
  }
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hard-Music</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.css" rel="stylesheet">

  </head>

  <body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <!--<span class="site-heading-upper text-primary mb-3">A Free Bootstrap 4 Business Theme</span>-->
      <span class="site-heading-lower">Hard-Musique</span>
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <!--<a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="interface.php">Article</a>
            </li>
            <li class="nav-item active px-lg-4 addart">
                <a class="nav-link text-uppercase text-expanded" href="add-article.php">Ajouté un article</a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="commentaire.php">commentaires</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
<section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 offset-lg-2 offset-md-1 offset-sm-1">
                        <p class="inp-reg">Nom de l'évenement :</p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <input type="text" name="nom" maxlength="250" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-3 offset-lg-3 offset-md-1 offset-sm-1">
                        <p class="inp-reg">Lieu :</p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <input type="text" name="lieu" maxlength="250" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-3 offset-lg-3 offset-md-1 offset-sm-1">
                        <p class="inp-reg">Début :</p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <input type="date" name="ddebut" required>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-1 offset-lg-1 offset-md-1 offset-sm-1">
                        <input type="time" name="tdebut" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-3 offset-lg-3 offset-md-1 offset-sm-1">
                        <p class="inp-reg">Fin :</p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <input type="date" name="dfin" required>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-1 offset-lg-1 offset-md-1 offset-sm-1">
                        <input type="time" name="tfin" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-3 offset-lg-3 offset-md-1 offset-sm-1">
                        <p class="inp-reg">Déscription :</p>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                    <textarea name="descrip" rows="10" cols="35" placeholder="Vous pouvez écrire quelque chose ici" required></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-3 offset-lg-3 offset-md-1 offset-sm-1">
                      <label for="fichier_a_uploader" title="Recherchez le fichier à uploader !">Envoyer une image :</label>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo MAX_SIZE; ?>" /><input name="fichier" type="file" id="fichier_a_uploader"/>
                    </div>
                </div>
                <div class="row">
                  <div class="col-sm-4 col-md-4 col-lg-1 offset-lg-5 offset-md-1 offset-sm-1">
                    <input class="btn btn-success" type="submit" name="ok" value="Enregistrez"/>
                    <?php if(isset($message)){echo $message;}?>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer text-faded text-center py-5">
      <div class="container">
        <p class="m-0 small">Copyright &copy; Your Website 2018</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>

<?php
if(isset($_POST['ok']) && !empty($_POST['nom']) && !empty($_POST['lieu']) && !empty($_POST['ddebut']) && !empty($_POST['tdebut']) && !empty($_POST['dfin']) && !empty($_POST['tfin']) && !empty($_POST['descrip'])){
  // Variable
  $nom=$_POST['nom'];
  $lieu=$_POST['lieu'];
  $descrip=$_POST['descrip'];
  $ddebut = $_POST['ddebut'];
  $dtime = $_POST['tdebut'];
  $dfin = $_POST['dfin'];
  $ftime = $_POST['tfin'];
// date debut
  $tt=explode(":", $dtime);
  $ddate=new DateTime($ddebut);
  $ddate->setTime($tt[0], $tt[1]);
  $ddateeok= $ddate->format('Y-m-d H:i:s') . "\n";
// date fin
  $tet=explode(":", $ftime);
  $fdate=new DateTime($dfin);
  $fdate->setTime($tet[0], $tet[1]);
  $fdateeok= $fdate->format('Y-m-d H:i:s') . "\n";

  if( !empty($_FILES['fichier']['name']) ){
    // Recuperation de l'extension du fichier
    $extension  = pathinfo($_FILES['fichier']['name'], PATHINFO_EXTENSION);
    // On verifie l'extension du fichier
    if(in_array(strtolower($extension),$tabExt)){
    // On recupere les dimensions du fichier
    $infosImg = getimagesize($_FILES['fichier']['tmp_name']);
    // On verifie le type de l'image
        if($infosImg[2] >= 1 && $infosImg[2] <= 14){
            // On verifie les dimensions et taille de l'image
            if(($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES['fichier']['tmp_name']) <= MAX_SIZE)){
            // Parcours du tableau d'erreurs
                if(isset($_FILES['fichier']['error']) && UPLOAD_ERR_OK === $_FILES['fichier']['error']){
                    // On renomme le fichier
                    $nomImage = sha1(uniqid()) .'.'. $extension;
                        // Si c'est OK, on teste l'upload
                    if(move_uploaded_file($_FILES['fichier']['tmp_name'], TARGET.$nomImage)){
                      $bdd = new PDO('mysql:host=127.0.0.1;dbname=hardmusic', 'root', '');
                      $sql=$bdd->prepare("INSERT INTO artevent157899 (nom, lieu, descrip, ddebut, dfin, nimg) VALUES (:nom, :lieu, :descrip, :ddebut, :dfin, :nimg)");
                      $sql->execute(array('nom' => $nom, 'lieu' => $lieu, 'descrip' => $descrip, 'ddebut' => $ddateeok, 'dfin' => $fdateeok, 'nimg' => $nomImage));
                      $message = 'Upload réussi !';
                    }else{
                    // Sinon on affiche une erreur systeme
                    $message = 'Problème lors de l\'upload !';
                    }
                }else{
                    $message = 'Une erreur interne a empêché l\'uplaod de l\'image';
                }
            }else{
            // Sinon erreur sur les dimensions et taille de l'image
            $message = 'Erreur dans les dimensions de l\'image !';
            }
        }else{
            // Sinon erreur sur le type de l'image
            $message = 'Le fichier à uploader n\'est pas une image !';
        }
    }else{
    // Sinon on affiche une erreur pour l'extension
    $message = 'L\'extension du fichier est incorrecte !';
    }
}
} else{

  $message = 'Veuillez remplir le formulaire svp !';
}
?>