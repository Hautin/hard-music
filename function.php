<?php 
// tester si l'utilisateur est identifié
// 0 = n'est pas connecter & 1 = est connecter
function detect_session($choice = 0) { 
	session_start();
	if(isset($_SESSION['user'])) { // connecté ?
        if($choice == 1) {
			$usercon=$_SESSION['user'];
    		header('Location: interface.php');
	    	exit;
    	}
    }
    else {
    	if($choice == 0) {
    		header('Location: admin.php');
	    	exit;
    	}
    }
}

// déconnexion de l'utilisateur
function disconnect_session() { 
	session_start();
	$_SESSION = array();
	session_destroy();
	header('Location: index.php');
	exit;
	
}
function bddconnect(){
	$bdd = new PDO('mysql:host=127.0.0.1;dbname=hardmusic', 'root', '');
	return $bdd;
}
function newutilisateur($mail1, $mail2, $mdp1, $mdp2){
	$bdd = bddconnect();
		if($mail1 == $mail2){
			if(filter_var($mail1, FILTER_VALIDATE_EMAIL)){
				$reqmail = $bdd->prepare("SELECT * FROM membre WHERE email = ?");
               	$reqmail->execute(array($mail1));
           		$mailexist = $reqmail->rowCount();
            	if($mailexist == 0) {
					if($mdp1 == $mdp2){
						//$envoie = insert_new_user_db($nom, $prenom, $mail, $mdp);
						$insertmbr = $bdd->prepare("INSERT INTO membre(email, mdp) VALUES(?, ?)");
						$insertmbr->execute(array($mail1, $mdp1));
                 		$erreur = "Votre compte a bien été créé !";
                 		
					} else{
						$erreur = "Les mots de passe ne sont pas identiques.";
						}
				} else {
                  	$erreur = "Un compte éxiste déjà avec cette adresse e-mail.";
               		}
			} else{
				$erreur = "Adresse e-mail invalide.";
				}
		} else{
			$erreur = "Les adresses e-mail ne sont pas identiques.";
			}
	return $erreur;
}
function newevent($nomev, $lieu, $descrip, $ddebut, $dfin, $nimg, $extenimg){
	$bdd=bddconnect();
	$sql=$bdd->prepare("INSERT INTO artevent157899 (nom, lieu, descrip, ddebut, dfin, nimg, extenimg) VALUES (:nom, :lieu, :descrip, :ddebut, :dfin, :nimg, :extenimg)");
	$sql->execute(array(':nom' => $nomev, ':lieu' => $lieu, ':descrip' => $descrip, ':ddebut' => $ddebut, ':dfin' => $dfin, ':nimg' => $nimg, ':extenimg' => $extenimg));
	$message = 'Upload réussi !';
	return $message;
}
?>