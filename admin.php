<?php 

require_once("function.php");
$user = detect_session(1); // tester si l'utilisateur est identifié ?
if(isset($_POST)){
	if(isset($_POST['log']) && !empty($_POST['mail']) && !empty($_POST['pass'])) {
		// traitement pour test de l'identification
		$loginconnect = htmlspecialchars($_POST['mail']);
	    $passconnect = sha1($_POST['pass']);
		$bdd = new PDO('mysql:host=127.0.0.1;dbname=hardmusic', 'root', '');
		$requs= $bdd -> prepare ('SELECT * FROM membre WHERE email=? AND mdp=?');
		$requs -> execute (array($loginconnect, $passconnect));
		$userexiste = $requs -> rowCount();
			if ($userexiste == 1) {
				# code...
				$userinfo = $requs->fetch();
				$_SESSION['user']= $userinfo['id_membre'];
				
				header("Location: interface.php");
			} else {
				$erreur= "Email ou mot de passe incorrect.";
			}
	} else {
		$erreur = "Merci de renseigner votre adresse e-mail et votre mot de passe.";
	}
}
?>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Hard-Music</title>

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/admin.css" rel="stylesheet">

</head>
<body>
<div class="">
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method='post'>
    <div class="login-page">
    <div class="form">
        <form >
            <div class="login-form">
                <input type="text" name="mail" placeholder="Email"/>
                <input type="password" name="pass" placeholder="Mot de passe"/>
            </div>
            
            <div class="except">
                <input class="btn btn-success" type="submit" name="log" value="login"/>
            </div>
             
        </form>
    </div>
</div>
		<div class="row">
			<?php
			if(isset($_POST['log'])){?>
				<div class="col-sm-10 col-md-10 col-lg-4 offset-lg-5 offset-md-1 offset-sm-1">
					<?php echo $erreur?>
				</div>
			<?php
			}
			?>
		</div>
	</form>
</div>
</body>