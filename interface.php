<?php

require_once("function.php");
$user = detect_session(); // tester si l'utilisateur est identifié ?
$bdd = new PDO('mysql:host=127.0.0.1;dbname=hardmusic', 'root', '');
$sql=$bdd->prepare("SELECT * FROM artevent157899");
$sql->execute();
$donne=$sql->fetchAll(PDO::FETCH_OBJ);
?>

<!DOCTYPE html>
<html lang="fr">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hard-Music</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.css" rel="stylesheet">

  </head>

  <body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <!--<span class="site-heading-upper text-primary mb-3">A Free Bootstrap 4 Business Theme</span>-->
      <span class="site-heading-lower">Hard-Musique</span>
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <!--<a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item active px-lg-4 art">
              <a class="nav-link text-uppercase text-expanded" href="interface.php">Article</a>
            </li>
            <li class="nav-item px-lg-4 addart">
                <a class="nav-link text-uppercase text-expanded" href="add-article.php">Ajouté un article</a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="commentaire.php">commentaires</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                      <th>Nom de l'événement</th>
                      <th>Début</th>
                      <th>Fin</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($donne as $res):?>
                  <tr>
                    <th><?=$res->nom?></th>
                    <th><?=date("d/m/Y".' à '."H:i", strtotime($res->ddebut));?></th>
                    <th><?=date("d/m/Y".' à '."H:i", strtotime($res->dfin));?></th>
                    <th><a href="edit.php"><img src="img/edit.png" alt="delete" id="icon-size-edit"></a><a href=""><img src="img/delete.png" alt="delete" id="icon-size-delete"></a></th>
                  </tr>
                  <?php endforeach;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer text-faded text-center py-5">
      <div class="container">
        <p class="m-0 small">Copyright &copy; Your Website 2018</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
